/*
Name of the project : Geolocation
File name : geo.js
Description : this program gives the current position of the user and
              bus_station,movie_theater,shopping_mall and taxi_stand
              close to the user using different icons.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
*/


var x = document.getElementById("demo");


function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(initMap);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}


//variable mar is used to change the color of the marker
var mar;
function initMap(position) {
    // The location of Uluru
    var pos = {lat: position.coords.latitude, lng: position.coords.longitude};
    // The map, centered at Uluru
    var map = new google.maps.Map(
    document.getElementById('map'), {zoom: 16, center: pos});
    // The marker, positioned at Uluru
    var marker = new google.maps.Marker({position: pos, map: map});
    infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);
    // use of google nearbySearch service to get nearby places
    //marker for movie_theater will be green
    mar='green';
    service.nearbySearch({
        location: pos,
        radius: 500,
        type: ['movie_theater']
    }, callback);
    //marker for bus_station will be blue
    mar='blue';
    service.nearbySearch({
        location: pos,
        radius: 500,
        type: ['bus_station']
    }, callback);
    //marker for shopping mall will be yellow
    mar='yellow';
    service.nearbySearch({
        location: pos,
        radius: 500,
        type: ['shopping_mall']
    }, callback);
    //marker for taxi_stand will be orange
    mar='orange';
    service.nearbySearch({
        location: pos,
        radius: 500,
        type: ['taxi_stand']
    }, callback);
}


function callback(results, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
        }
    }
}


function createMarker(place) {
    var placeLoc = place.geometry.location;
    //mark the current position of the user using default marker
    var marker = new google.maps.Marker({
        icon : mar,
        map: map,
        position: place.geometry.location
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
    });
}
